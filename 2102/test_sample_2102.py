from sample_2102 import connect_db
from unittest.mock import MagicMock, patch

@patch('sample_2102.psycopg2.connect')
def test_success(mock_connect):

    columns = [
        ['id'],
        ['name'],
        ['created_at'],
        ['updated_at'],
    ]
    data = [
        ['1', 'A', 'created_at', 'updated_at'],
        ['2', 'B', 'created_at', 'updated_at']
    ]

    _mock_connect = mock_connect.return_value
    _mock_cur = _mock_connect.cursor()
    
    _mock_cur.description = columns
    _mock_cur.fetchall.return_value = data

    result = connect_db()
    assert result.get('name')[0] == data[0][1]
    assert result.get('name')[1] == data[1][1]

@patch('sample_2102.psycopg2.connect')
def test_failure(mock_connect):

    columns = [
        ['id'],
        ['name'],
        ['created_at'],
        ['updated_at'],
    ]
    _mock_connect = mock_connect.return_value
    _mock_cur = _mock_connect.cursor()
    
    _mock_cur.description = columns
    _mock_cur.fetchall.side_effect = MagicMock(side_effect=TimeoutError)

    result = connect_db()
    assert result == ""
