import pandas as pd
import psycopg2

def connect_db():
    conn = ""
    cursor = ""
    data_frame = ""
    cert_file_path = "dummy"
    try:
        conn = psycopg2.connect(host="REDSHIFT_HOST", 
                                port="POST", 
                                user="REDSHIFT_USER", 
                                password="REDSHIFT_PASSWORD",
                                sslmode="SSL_MoDE", 
                                sslrootcert=cert_file_path, 
                                database="DATABASE", 
                                connect_timeout="CONNECT_TIMEOUT")
        cursor = conn.cursor()

        query = "CERNER_MNEMONIC_EXTRACT_QUERY"
        cursor.execute(query)
        data = cursor.fetchall()
        cols = []
        for item in cursor.description:
            cols.append(item[0])

        data_frame = pd.DataFrame(data=data, columns=cols)
        data_frame = data_frame.drop(
            ['id', 'created_at', 'updated_at'], axis=1)
    except Exception as error:
        print("Exception", error)
    finally:
        cursor.close()
        conn.close()
    return data_frame
