from sample import get_data_1, get_data_2

def test_get_data(requests_mock):
    # mock API 1
    requests_mock.get(
        'https://jsonplaceholder.typicode.com/todos/1',
        text='{"userId":1,"id":1,"title":"delectus aut autem","completed":false}')

    # mock API 2
    requests_mock.get(
        'https://jsonplaceholder.typicode.com/todos/2',
        text='{"userId":1,"id":2,"title":"delectus aut autem","completed":false}')

    response1 = get_data_1()
    response2 = get_data_2()
    
    data1 = response1.json()
    data2 = response2.json()

    assert data1['id'] == 1
    assert data2['id'] == 2