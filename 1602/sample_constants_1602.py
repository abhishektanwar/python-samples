NEW_CONFIG = {
    "base_url": "https://github.cerner.com/api/v3",
    "repo_list": []
}

JSON = {
    "repo_name": "",
    "branch": "master",
    "file_path_list": [
        {
            "file_path": "",
            "file_name": [
                "healthe-care-algorithms.csv"
            ],
            "s3_directory": "github_extracted_csv_files"
        }
    ]
}

CONFIG_CLIENT_SCHEMA = "client_schema"
REPO_NAME = "repo_name"
FILE_PATH_LIST = "file_path_list"
FILE_PATH = "file_path"
REPO_LIST = "repo_list"