from sample_constants_1602 import NEW_CONFIG, JSON, CONFIG_CLIENT_SCHEMA, REPO_NAME, FILE_PATH_LIST, FILE_PATH, REPO_LIST

import copy

def client_json_formatter(client_object):
    client_list_config = NEW_CONFIG
    # This is wrong
    # json_obj = JSON
    print(client_object[CONFIG_CLIENT_SCHEMA])
    for client in client_object[CONFIG_CLIENT_SCHEMA]:
        # This is required now to instantiate a copy of JSON by using .copy
        # since this is nested object of objects
        # you need to do a deep copy
        json_obj = copy.deepcopy(JSON)
        client_url = client.split("/")
        client_name = "Data-Intelligence/" + client_url[1]
        client_file_string = client_url[2:-1]
        client_file_path = "/".join(client_file_string) + "/"
        json_obj[REPO_NAME] = client_name
        json_obj[FILE_PATH_LIST][0][FILE_PATH] = client_file_path
        # if json_obj not in client_list_config[REPO_LIST]:
        client_list_config[REPO_LIST].append(json_obj)
    print(client_list_config)
    return client_list_config


client_object = {
    CONFIG_CLIENT_SCHEMA: ['./test1/test1/test1.csv',
                           './test2/test2/test2.csv']
}
client_json_formatter(client_object)
