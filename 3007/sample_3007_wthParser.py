# The above class is a custom HTML parser in Python that converts HTML tags into formatted text in a
# Word document using the `docx` library.
from docx import Document
from html.parser import HTMLParser

html = """
<h2>Complex Paragraph</h2>
<p>A Paragraph with <b>Bold</b>, <i>Itatics</i></p>
"""

class MyHTMLParser(HTMLParser):

    current_tag = None
    current_runner = None
    stack = []
    def handle_starttag(self, tag, attrs):
        # add to tag stack
        
        if tag == "h2":
            self.current_tag = document.add_heading("", level=2)
        if tag == "p":
            self.current_tag = document.add_paragraph("")

        self.stack.append(self.current_tag)

    def handle_endtag(self, tag):
        # remove last pushed tag
        self.current_tag = self.stack.pop()
        if tag == "b":
            self.current_runner.bold = True
        if tag == "i":
            self.current_runner.italic = True

        self.current_tag = None

    def handle_data(self, data):
        if self.current_tag != None and data != "\n":
            self.current_runner = self.current_tag.add_run(data)

document = Document()
document.add_heading('Document', 0)

# para = document.add_paragraph("")
# runner = para.add_run('Normal')
# para.add_run('Bold')
# runner.bold = True
# runner = para.add_run('Italics')
# runner.italic = True

parser = MyHTMLParser()
parser.feed(html)

document.save('sample_3007_withParser.docx')