from docx import Document
from htmldocx import HtmlToDocx

document = Document()
new_parser = HtmlToDocx()

# set table grid
new_parser.table_style = 'Table Grid'

html = """
<h2>Heading</h2>

<h2>Paragraph</h2>
<p>
<b>Lorem</b> <i>ipsum</i> dolor sit amet, <u>consectetur</u> adipiscing elit, sed do eiusmod tempo
</p>

<h2>Link</h2>
<p>
    <a href="">Sample Link</a>
</p>

<h2>Table</h2>
<table>
    <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1</td>
            <td>Abhishek</td>
        </tr>
    <tbody>
</table>
"""


new_parser.add_html_to_document(html, document)

# do more stuff to document
document.save('sample3007.docx')