import requests
import bearer

def get_jira_data():
    token = bearer.get_token()
    if token == None:
        return None
    try:
        response = requests.get("https://jsonplaceholder.typicode.com/todos/1")
    except (TimeoutError):
        return None
    return response.json()