from unittest.mock import MagicMock, Mock, patch
from sample1 import get_jira_data

@patch('sample1.requests')
@patch('sample1.bearer')
def test_get_jira_data_success(mock_bearer, mock_requests):

    def mock_side_effect(url):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.json.return_value = {
            "id": 1
        }
        return mock_response
    
    mock_requests.get.side_effect = mock_side_effect
    mock_bearer.get_token.return_value = 'BEARER'

    data1 = get_jira_data()

    assert data1['id'] == 1

@patch('sample1.requests')
@patch('sample1.bearer')
def test_get_jira_data_bearer_failure(mock_bearer, mock_requests):

    def mock_side_effect(url):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.json.return_value = {
            "id": 1
        }
        return mock_response
    
    mock_requests.get.side_effect = mock_side_effect
    mock_bearer.get_token.return_value = None

    data1 = get_jira_data()

    assert data1 == None

@patch('sample1.requests')
@patch('sample1.bearer')
def test_get_jira_data_request_failure(mock_bearer, mock_requests):

    mock_requests.get.side_effect = MagicMock(side_effect=TimeoutError)
    mock_bearer.get_token.return_value = 'BEARER'

    data1 = get_jira_data()

    assert data1 == None