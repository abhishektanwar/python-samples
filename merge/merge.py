resp_zone1_json = {
    'items': [
        {
            'cernerMnemonics': ['A', 'B']
        },
        {
            'cernerMnemonics': ['B', 'C']
        }
    ]
}

resp_zone2_json = {
    'items': [
        {
            'cernerMnemonics': ['C', 'D']
        },
        {
            'cernerMnemonics': ['D', 'E']
        }
    ]
}

def merge_item_mnemonics_unique(array, response):
    for item in response["items"]:
        for item_mnemonics in item["cernerMnemonics"]:
            if item_mnemonics not in final_mnemonics:
                array.append(item_mnemonics)

final_mnemonics = []

merge_item_mnemonics_unique(final_mnemonics, resp_zone1_json)
merge_item_mnemonics_unique(final_mnemonics, resp_zone2_json)


print(final_mnemonics)
