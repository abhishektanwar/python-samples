from unittest.mock import MagicMock, patch
from sample import get_data_1, get_data_2


@patch('sample.requests')
def test_get_data(mock_requests):

    def mock_side_effect(url):
        mock_response = MagicMock()
        mock_response.status_code = 200
        if url == "https://jsonplaceholder.typicode.com/todos/1":
            mock_response.json.return_value = {
                'id': 1,
            }
        elif url == "https://jsonplaceholder.typicode.com/todos/2":
            mock_response.json.return_value = {
                'id': 2,
            }
        else:
            mock_response.json.return_value = {
                'id': 3,
            }
        return mock_response

    mock_requests.get.side_effect = mock_side_effect

    response1 = get_data_1()
    data1 = response1.json()

    response2 = get_data_2()
    data2 = response2.json()

    assert data1['id'] == 1
    assert data2['id'] == 2
