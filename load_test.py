import threading
import requests

# Define the API endpoint you want to call
API_URL = "https://jsonplaceholder.typicode.com/todos/1"

# Define the number of threads and API calls
num_threads = 10
num_api_calls = 10

# Define a function to make API calls
def make_api_call(thread_num, api_num):
    try:
        response = requests.get(API_URL)
        if response.status_code == 200:
            print(f"Thread {thread_num}, API Call {api_num}: Success")
        else:
            print(f"Thread {thread_num}, API Call {api_num}: Failed (Status Code {response.status_code})")
    except Exception as e:
        print(f"Thread {thread_num}, API Call {api_num}: Error - {str(e)}")

# Create and start threads
threads = []

for i in range(num_threads):
    for j in range(num_api_calls):
        thread = threading.Thread(target=make_api_call, args=(i+1, j+1))
        threads.append(thread)
        thread.start()

# Wait for all threads to finish
for thread in threads:
    thread.join()

print("All API calls have been completed.")