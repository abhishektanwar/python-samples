import requests

def download_large_github_file(auth_username: str, auth_password: str, file_url: str) -> bytes:
    try:
        gh_session = requests.Session()
        gh_session.auth = (auth_username, auth_password)
        output = gh_session.get(file_url).content
        return output
    except Exception:
        return "Error"