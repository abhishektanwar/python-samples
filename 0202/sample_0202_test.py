from unittest.mock import MagicMock, patch
from sample_0202 import download_large_github_file

@patch('sample_0202.requests')
def test_download_large_github_file_success(mock_requests):

    class MyObject:
        content = bytes("SAMPLE", "utf-8")
    
    returnObj = MyObject()
    mock_requests.Session().get.return_value = returnObj
    data = download_large_github_file("user", "pass", "file")
    assert data == bytes("SAMPLE", "utf-8")

@patch('sample_0202.requests')
def test_download_large_github_file_failure(mock_requests):
    mock_requests.Session().get.side_effect = MagicMock(side_effect=TimeoutError)
    data = download_large_github_file("user", "pass", "file")
    assert data == "Error"
    