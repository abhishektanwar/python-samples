import datetime

def get_comment():
    DATE_FORMAT = "%d/%m/%Y"
    today = datetime.date.today().strftime(DATE_FORMAT)
    comment = 'Comment on '+today
    return comment