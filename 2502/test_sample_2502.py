from sample_2502 import get_comment
from unittest.mock import MagicMock, patch

@patch('sample_2502.datetime')
def test_success(mock_dt):
    mock_dt.date.today().strftime.return_value = 'TEST'
    expect = 'Comment on TEST'
    result = get_comment()
    assert result == expect