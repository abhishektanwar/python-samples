import requests

def get_data_1():
  response = requests.get("https://jsonplaceholder.typicode.com/todos/1")
  return response

def get_data_2():
  response = requests.get("https://jsonplaceholder.typicode.com/todos/2")
  return response

