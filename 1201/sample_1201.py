from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


def set_msg_contents(YAML_DATA, status_flag, text_msg):
    msg = MIMEMultipart()
    body = ""
    recipients = YAML_DATA.get('EMAIL_RECEIVERS_USER_ID')
    msg['From'] = YAML_DATA.get('EMAIL_SENDERS_USER_ID')
    msg['To'] = ", ".join(recipients)
    if status_flag == 1:
        msg['Subject'] = YAML_DATA.get('EMAIL_SUBJECT_SUCCESS')
        body = YAML_DATA.get("EMAIL_BODY_SUCCESS") + "\n\n" + text_msg
    elif status_flag == 0:
        msg['Subject'] = YAML_DATA.get('EMAIL_SUBJECT_FAILURE')
        body = YAML_DATA.get('EMAIL_BODY_FAILURE') + "\n\n" + text_msg
    elif status_flag == 2:
        msg['Subject'] = YAML_DATA.get('EMAIL_SUBJECT_RETRY_FAILURE')
        body = YAML_DATA.get('EMAIL_BODY_RETRY_FAILURE') + "\n\n" + text_msg
    elif status_flag == 3:
        msg['Subject'] = YAML_DATA.get('EMAIL_SUBJECT_IN_PROGRESS')
        body = YAML_DATA.get('DATA_SYNDICATION_IN_PROGRESS')
    
    msg.attach(MIMEText(body, "plain"))
    return msg
