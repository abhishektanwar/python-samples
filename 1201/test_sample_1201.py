from sample_1201 import set_msg_contents

YAML_DATA = {
    'EMAIL_SENDERS_USER_ID': 'EMAIL_SENDERS_USER_ID',
    'EMAIL_SUBJECT_SUCCESS': 'EMAIL_SUBJECT_SUCCESS',
    'EMAIL_SUBJECT_FAILURE': 'EMAIL_SUBJECT_FAILURE',
    'EMAIL_SUBJECT_RETRY_FAILURE': 'EMAIL_SUBJECT_RETRY_FAILURE',
    'DATA_SYNDICATION_IN_PROGRESS': 'DATA_SYNDICATION_IN_PROGRESS',
    'EMAIL_RECEIVERS_USER_ID': ['a@a.com'],
    'EMAIL_BODY_FAILURE': 'EMAIL_BODY_FAILURE',
    'EMAIL_SUBJECT_IN_PROGRESS': 'EMAIL_SUBJECT_IN_PROGRESS',
    'EMAIL_BODY_RETRY_FAILURE': 'EMAIL_BODY_RETRY_FAILURE',
    'EMAIL_BODY_SUCCESS': 'EMAIL_BODY_SUCCESS'
}

def test_set_msg_contents_status_0():
    msg = set_msg_contents(YAML_DATA, status_flag=0, text_msg='Hello')
    assert msg['From'] == YAML_DATA.get('EMAIL_SENDERS_USER_ID')
    assert msg['Subject'] == YAML_DATA.get('EMAIL_SUBJECT_FAILURE')

def test_set_msg_contents_status_1():
    msg = set_msg_contents(YAML_DATA, status_flag=1, text_msg='Hello')
    assert msg['From'] == YAML_DATA.get('EMAIL_SENDERS_USER_ID')
    assert msg['Subject'] == YAML_DATA.get('EMAIL_SUBJECT_SUCCESS')

def test_set_msg_contents_status_2():
    msg = set_msg_contents(YAML_DATA, status_flag=2, text_msg='Hello')
    assert msg['From'] == YAML_DATA.get('EMAIL_SENDERS_USER_ID')
    assert msg['Subject'] == YAML_DATA.get('EMAIL_SUBJECT_RETRY_FAILURE')

def test_set_msg_contents_status_3():
    msg = set_msg_contents(YAML_DATA, status_flag=3, text_msg='Hello')
    assert msg['From'] == YAML_DATA.get('EMAIL_SENDERS_USER_ID')
    assert msg['Subject'] == YAML_DATA.get('EMAIL_SUBJECT_IN_PROGRESS')

def test_set_msg_contents_status_none():
    msg = set_msg_contents(YAML_DATA, status_flag=100, text_msg='Hello')
    assert msg['From'] == YAML_DATA.get('EMAIL_SENDERS_USER_ID')
    assert msg['Subject'] == None