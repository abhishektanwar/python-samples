# Constants
EMPTY_ARRAY = []

def get_config(github_keys):
    return {
        "base_url": github_keys["base_url"],
        "repo_list": EMPTY_ARRAY
    }