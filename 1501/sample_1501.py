import json
import pandas as pd

def http_put_call(url, payload_json_formatted, yam_data):
    return 'response'

ADD_PRIORITY_LOG = 'ADD_PRIORITY_LOG'
JIRA_NUMBER_COLUMN = 'jira_number'
ISSUE_URL = 'ISSUE_URL'
PRIORITY_URL = 'PRIORITY_URL'
PRIORITY_PAYLOAD = {
    'FIELDS': {
        'PRIORITY_FIELD': 'PRIORITY_FIELD'
    }
}
PRIORITY_JSON_VALUES = 'PRIORITY_JSON_VALUES'
PRIORITY_VALUE = 'PRIORITY_VALUE'
URL = 'URL'
FIELDS = 'FIELDS'
PRIORITY_FIELD = 'PRIORITY_FIELD'

def add_priority_to_jira(data, yaml_data):
    try:
        for jira_key in data[JIRA_NUMBER_COLUMN]:
            url = yaml_data[URL] + ISSUE_URL + jira_key + PRIORITY_URL
            payload = PRIORITY_PAYLOAD
            priority_json_value = yaml_data[PRIORITY_JSON_VALUES][PRIORITY_VALUE]
            payload[FIELDS][PRIORITY_FIELD] = priority_json_value
            payload_json_formatted = json.dumps(payload)
            response = http_put_call(url, payload_json_formatted, yaml_data)
            print(str (response))
    except AttributeError as err:
        print(str (err))