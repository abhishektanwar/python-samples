from sample_1501 import add_priority_to_jira
from unittest.mock import MagicMock, Mock, patch
import pandas as pd

@patch('sample_1501.http_put_call')
def test_add_priority_to_jira_success(mock_http_put_call):
    d = {'jira_number': ['1100', '1101'], 'mock_avg': [3, 4]}
    data = pd.DataFrame(data=d)

    mock_http_put_call.return_value = "put call"
    add_priority_to_jira(data=data, yaml_data={
        'URL': 'URL',
        'PRIORITY_JSON_VALUES': {
            'PRIORITY_VALUE': 'PRIORITY_VALUE'
        }
    })

@patch('sample_1501.http_put_call')
def test_add_priority_to_jira_fail(mock_http_put_call):
    d = {'jira_number': ['1100', '1101'], 'mock_avg': [3, 4]}
    data = pd.DataFrame(data=d)

    mock_http_put_call.side_effect = MagicMock(
        side_effect=AttributeError('AttributeError')
    )

    add_priority_to_jira(data=data, yaml_data={
        'URL': 'URL',
        'PRIORITY_JSON_VALUES': {
            'PRIORITY_VALUE': 'PRIORITY_VALUE'
        }
    })
